<!--
.. title: Why I Prefer Vim to Neovim
.. slug: why-i-prefer-vim-to-neovim
.. date: 2023-07-31 23:41:17 UTC+02:00
.. tags: vim, vimrc, neovim, helix, copilot, editor
.. category: Linux
.. link: 
.. description: Vim provides me a better experience than Neovim and here's why.
.. type: text
-->

Vim needs no introduction. However, what you might not be aware of is that there was a period when its development was stagnant. To address this issue, [Neovim](https://github.com/neovim/neovim) was born with the goal of creating an easier-to-maintain codebase, encouraging more community involvement, and adopting new features more rapidly. It made quite a splash several years ago.

After reading about Neovim for years, I finally decided to give it a try. The promises were enticing - faster plugins thanks to Lua, improved responsiveness, better syntax highlighting, and more. Once I started using it, I immediately noticed a snappier performance, more sensible default, and effortless pasting without needing to enable `:paste`. Impressed, I began adapting my `vimrc` to Neovim and sought Lua alternatives for the plugins I had used.
<!-- TEASER_END -->

However, after spending nearly two days tweaking things, I realized going back to Vim is the better choice. It dawned on me that trying to mold Neovim to my preferences when my Vim setup was already working perfectly wasn't worth the effort. No doubt, in path, because I found Neovim's plugins more challenging to configure, with LSP & autocomplete being too fiddly, broken configuration examples in the documentation, and color schemes not playing well with other plugins despite claims otherwise. Moreover, many of Neovim's flagship features have now been integrated into Vim as well, such as pop-up windows, async support, and a built-in terminal. Additionally, the Neovim version available in repositories on several operating systems was outdated, leaving me with no choice but to either abandon certain plugins or install Neovim from elsewhere.

In conclusion, my decision to stick with Vim stems from its hassle-free experience. Now, there's another editor called [Helix](https://github.com/helix-editor/helix) that truly excites me. I'm hoping they will soon support Github Copilot, allowing me to give it a proper try.
