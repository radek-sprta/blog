<!--
.. title: Blog Terminal - A Wordpress Terminal Plugin
.. slug: blog-terminal-wordpress-terminal-plugin
.. date: 2017-11-18 15:09:30
.. tags: blog terminal,command line,plugin,terminal,wordpress
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Unsorted
.. has_math: no
.. status: published
.. wp-status: publish
-->

You might have noticed a new entry quietly appearing on the [project
page](https://radeksprta.eu/projects/). So, let me introduce it to you. The new
entrant is [Blog Terminal](https://wordpress.org/plugins/blog-terminal/), a
Wordpress plugin for drawing a terminal-like box, complete with prompt. If you
have read some of my other blog posts, Blog Terminal is what powers the
terminal boxes in them.  For the longest time I have used Post Terminal plugin
by Brandon Griffith. But there were some things I did not like about it.
Namely, it was not possible to draw a terminal without a prompt (for showing
example configs). Furthermore, since it did not take advantage of many of the
Wordpress APIs, the code was unnecessarily complicated. This made it hard to
add new features. Therefore, I have decided to completely rewrite it.
<!-- TEASER_END -->

## Features

The result is much leaner and easier to maintain code base, that tries to use
Wordpress API's as much as possible. The unwieldy syntax was replaced by
[[terminal][/terminal]] shortcode. In addition, lines in terminal are now
separated by regular newlines, instead of HTML tags. And prompt now only shows
on the lines starting with '$'. Finally, the default look is more minimalist:

```text
$ echo "Blog Terminal"
```

## Conclusion

To learn more about Blog Terminal, see its [Wordpress
page](https://wordpress.org/plugins/blog-terminal/). If you have ideas for the
plugin, want to report a bug or help with the development, head over to the
[project page](https://gitlab.com/radek-sprta/vigil).
