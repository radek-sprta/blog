<!--
.. title: Automatically Remove Old Kernels in Ubuntu
.. slug: automatically-remove-old-kernels-ubuntu
.. date: 2015-08-25 08:59:57
.. tags: bash,kernel,script,ubuntu
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Couple versions ago, Ubuntu has stopped to automatically remove old kernels.
It just ignores the setting for this functionality. As a result, after several
years od usage, the system gets seriously cluttered. In some cases, it can
even run out of space. In order to prevent this, I made a [simple bash
script](https://gitlab.com/radek-sprta/kernel-cleanup) to get rid of old
kernels.  You can run it manually, but I prefer having it on a monthly cron
job and forgetting about it. By default, it leaves the two most recent kernels
intact. This serves as a safety catch in case kernel upgrade doesn't go well.
However, you can adjust this to your needs. If you have any questions about
usage, just leave me a comment.

