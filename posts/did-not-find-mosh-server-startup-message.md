<!--
.. title: Did not find mosh server startup message
.. slug: did-not-find-mosh-server-startup-message
.. date: 2019-11-24 12:44:05
.. tags: error,fix,locale,mosh,ssh
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

If you have just [installed Mosh server](https://radeksprta.eu/setup-mosh-sshs-cousin-for-mobile-era/)
and tried to connect to it, you might have run into the following error:

```bash
$ mosh user@server
/usr/bin/mosh: Did not find mosh server startup message.
```

This happens when your SSH
session sends a locale that Mosh does not support. The fix is fairly easy,
just configure your SSH not to send the LANG variable. To do that, open your
`/etc/ssh/ssh_config` and comment out the following line:

```text
# SendEnv LANG LC_*
```

That's it, now you can use your Mosh without any hiccups.

