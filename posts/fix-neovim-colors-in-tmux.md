<!--
.. title: Fix Neovim Colors in Tmux
.. slug: fix-neovim-colors-in-tmux
.. date: 2025-01-15 00:36:27 UTC+01:00
.. tags: neovim, tmux, rgb, true colors, theme, TIL
.. category: Linux
.. link: 
.. description: Learn how to configure Tmux with a simple tweak to ensure seamless 24-bit color rendering in your terminal, so your Neovim themes work correctly.
.. type: text
-->

When I switched to Neovim, I ran into an unexpected issue. My favorite theme, [Solarized](https://ethanschoonover.com/solarized/), did not display correctly. Strangely enough, other schemes worked just fine.

After some investigation, I realized this only happened within a Tmux session. I always assumed setting `$TERM` to `screen-256color` was sufficient for modern color rendering. But it turns out, there is one more step to enable proper colors in Tmux.

## The Fix

To get Solarized (or any other TrueColor Neovim scheme) working, you need to enable the following option to your Tmux configuration:

```
set -as terminal-features ',xterm*:RGB'
```

## Why This Works
This arcane invocation tells Tmux that your terminal [supports 24-bit TrueColor](https://github.com/tmux/tmux/wiki/FAQ#how-do-i-use-rgb-colour). Without this setting, Tmux defaults to 256-color mode, which may not render certain themes correctly.

In the terminfo specification, the [RGB](https://github.com/termstandard/colors?tab=readme-ov-file#checking-for-colorterm) capability indicates TrueColor support. This line explicitly enables that feature for terminals that match the pattern xterm*.

## Verifying TrueColor Support

To ensure your terminal supports TrueColor, run the following command:

```bash
echo -e "\e[48;2;255;0;0m   \e[48;2;0;255;0m   \e[48;2;0;0;255m   \e[0m"
```

If the colors render correctly, your terminal is ready for TrueColor.


## Conclusion

With this tweak, the Solarized theme now works perfectly in Neovim within Tmux. If you're using a modern terminal, make sure to enable TrueColor for the best visual experience. Have fun customizing your setup!
