<!--
.. title: Baldur's Gate Expansion Is in The Works
.. slug: baldurs-gate-expansion
.. date: 2015-07-17 23:41:16
.. tags: dlc,rpg
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games
.. has_math: no
.. status: published
.. wp-status: publish
-->

[Beamdog](https://www.beamdog.com "Beamdog homepage") is a company that has
brought new life to [Baldur's Gate](https://radeksprta.eu/baldurs-gate-in-japanese/ "Baldur’s Gate in Japanese")
and other Infinity games with their
Enhanced Editions. Recently, they have announced a new Baldur's Gate expansion
- Siege of Dragonspear. This expansion will take place between the events of
Baldur's Gate 1 and 2. The story revolves around the Shining Lady, a
mysterious figure from the North, who leads a crusade down the Sword Coast.
Siege of Dragonspear will feature several new locations, a new shaman class,
four new companions and a plethora of other content. In addition, there are
also plans for localization. So here is some hoping for a Japanese version.
Some 16 years after the last game in the series, this is a long-awaited
sequel. I, for one, am looking forward to it. 

{{% media url="https://youtu.be/pRv8bqeR_T4" %}}
