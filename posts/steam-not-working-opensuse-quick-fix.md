<!--
.. title: Steam Not Working on OpenSuse? Here's a Quick Fix
.. slug: steam-not-working-opensuse-quick-fix
.. date: 2017-11-12 11:24:54
.. tags: bug,fix,libgl_dri3,opensuse,steam
.. category: Games
.. link:
.. description:
.. type: text
.. categories: Games,Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

After a couple of busy weeks, I finally found myself with some free time.
There were a couple of interesting game releases that I wanted to check out.
Unfortunately, that did not go so well in OpenSuse, with Steam not working. I
was welcomed by the following error:

```text
symbol lookup error: /usr/lib/libxcb-dri3.so.0:
undefined symbol: xcb_send_request_with_fds
```

It seems that Steam does not work as
smooth in OpenSuse as in Ubuntu. Luckily, the fix was quite easy. You can
disable the library in question using an environment variable. So I just set
it when launching Steam:

```bash
$ host +x
$ LIBGL_DRI3_DISABLE=1 steam
```
Once I did that, the Steam launched normally and updated
itself. After that, the problem disappeared, therefore I could just can run
Steam normally. Time to try out
[Shieldbreaker](https://www.gamesear.com/news/darkest-dungeon-developers-have-just-announced-a-new-class-the-shieldbreaker),
the new Darkest Dungeon DLC, with [Steam 
controler](https://radeksprta.eu/posts/using-steam-controller-in-ubuntu/).
If you still have trouble running Steam, [this might help
you](https://radeksprta.eu/posts/fix-steam-ubuntu-without-hassle/). Meanwhile, here's hoping
that the [Steam Linux Integration ](https://github.com/solus-project/linux-steam-integration)
project comes to Opensuse as well, for a seamless Steam
experience.
