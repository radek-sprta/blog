<!--
.. title: Import Scaleway Infrastructure to Terraform
.. slug: import-scaleway-infrastructure-to-terraform
.. date: 2020-11-03 11:28:43 UTC+02:00
.. tags: terraform, scaleway, import, infrastructure, iaas
.. category: DevOps
.. link: 
.. description: This article will teach you how to import Scaleway infrastructure to Terraform, so you can manage it as code.
.. type: text
-->

[Terraform](https://www.terraform.io) is one of the leading applications to manage your infrastructure as code. It defines your server instances and accompanying services using a simple declarative language. Moreover, the infrastructure state is kept in a separate file. So, whenever you make a change in your configuration, Terraform compares it to the current state and only performs necessary changes.

Terraform has plugins for all the major IaaS providers, so you should be covered there. However, you are probably not starting from a scratch, but already have some infrastructure running. Personally, I use [Scaleway](https://www.scaleway.com) as my cloud provider, so I will show you how to import their resources to Terraform. I will demonstrate the process on a single server instance.
<!-- TEASER_END -->

## Terraform installation

Terraform provides statically compiled binaries for various platforms. You can download them [here](https://www.terraform.io/downloads.html). Afterwards, just move the file to a directory in your `$PATH` (for example `/usr/local/bin`). Check that it works by running `terraform version`.

## Generate Scaleway API key

In order to use Terraform with Scaleway, you need to create an API key, consisting of an access key and a secret key. To generate one, log to [Scaleway Console](https://console.scaleway.com), click on the user menu and choose **Credentials**.

![Scaleway user menu](/images/scaleway_apikey1.png)

Once in the **Credentials** page, click on **Generate new API key**.

![API key menu](/images/scaleway_apikey2.png)
<screenshot2>

Choose a suitable name, such as *Terraform Key*, and **Generate API Key**.

![Choose key name](/images/scaleway_apikey3.png)

## Import instrastructure

Before you can start importing infrastructure, you need to create a Terraform configuration file, let's call it `scaleway.tf`. Use it to define the resources you want to import. For a single server instance, that's `scaleway_instance_server`, `scaleway_instance_volume` and `scaleway_instance_ip`. Moreover, you need to include your newly generated credentials. The configuration file should look similar to the example below.

```terraform
provider "scaleway" {
  access_key      = "<SCALEWAY-ACCESS-KEY>"
  secret_key      = "<SCALEWAY-SECRET-KEY>"
  organization_id = "<SCALEWAY-ORGANIZATION-ID>"
  zone            = "fr-par-1"
  region          = "fr-par"
}

resource "scaleway_instance_ip" "public_ip" {}

resource "scaleway_instance_volume" "data" {}

resource "scaleway_instance_server" "opensuse_instance" {}
```

Now, initiate Terraform by running:

`terraform init`

It will download the necessary plugins to use Scaleway. Finally, you can start importing the individual pieces of infrastructure. The command for importing the server instance would be:

`terraform import scaleway_instance_server.opensuse_instance fr-par-1/<instance_id>`

However, if you check the configuration file, you will notice it stayed the same. The `import` command imports the infrastructure state, but does not modify the configuration to reflect it. You have to do this step yourself. (Be careful not to run Terraform before you do, because it would effectively destroy your infrastructure.)

The easiest way is to print the current state by running:

`terraform show scaleway_instance_server.opensuse_instance`

And copy the details to the `opensuse_instance` section in `scaleway.tf`. Not all keys are necessary, please refer to the [documentation](https://registry.terraform.io/providers/scaleway/scaleway/latest/docs/resources/instance_server) for the details. Once you are done, repeat the same procedure for `scaleway_instance_volume` and `scaleway_instance_ip`. In the end, your configuration file should look like this:


```terraform
provider "scaleway" {
  access_key      = "<SCALEWAY-ACCESS-KEY>"
  secret_key      = "<SCALEWAY-SECRET-KEY>"
  organization_id = "<SCALEWAY-ORGANIZATION-ID>"
  zone            = "fr-par-1"
  region          = "fr-par"
}

resource "scaleway_instance_ip" "public_ip" {
    organization_id = "<SCALEWAY-ORGANIZATION-I>"
    zone            = "fr-par-1"
}

resource "scaleway_instance_volume" "data" {
    name            = "<SNAPSHOT-NAME>"
    organization_id = "<SCALEWAY-ORGANIZATION-ID>"
    size_in_gb      = 20
    type            = "l_ssd"
    zone            = "fr-par-1"
}

resource "scaleway_instance_server" "opensuse_instance" {
    additional_volume_ids = []
    enable_dynamic_ip     = false
    enable_ipv6           = true
    image                 = "fr-par-1/<SCALEWAY_IMAGE>"
    ip_id                 = scaleway_instance_ip.public_ip.id
    name                  = "opensuse_instance"
    state                 = "started"
    tags                  = []
    type                  = "DEV1-S"
    zone                  = "fr-par-1"

    root_volume {
        delete_on_termination = true
    }

    user_data {
        key   = "ssh-host-fingerprints"
        value = "<SSH KEY>"
    }
}
```

Check that you have entered everything correctly by running:

`terraform plan`.

If the configuration corresponds with the Terraform state, it should return that no changes are necessary.

## Conclusion

Importing the infrastructure to Terraform might be a bit of a time-consuming process. But afterwards, you will be ready to reap all the benefits of infrastructure as code, such as consistency and reproducibility. If want learn more about Terraform, check its [documentation](https://www.terraform.io/docs/index.html).
