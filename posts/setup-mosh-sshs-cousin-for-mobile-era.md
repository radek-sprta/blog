<!--
.. title: Setup Mosh - SSH's Cousin for Mobile Era
.. slug: setup-mosh-sshs-cousin-for-mobile-era
.. date: 2017-06-27 13:28:52
.. tags: mobile,mosh,server,ssh,tutorial
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

SSH, while an indispensable tool, is starting to show its age in certain
situations. We have all experienced the lag when using cell phone tethering or
finding your remote connections hanging in limbo when resuming your laptop
from sleep. No wonder - the first release of SSH came out over twenty years
ago and the landscape was very different back then. It was an age of desktop
workstations and cable networks. When SSH was designed, nobody thought people
would be working on laptops, switching between wifis and mobile connections.
Mosh - short for mobile shell - aims at those users. It uses state machines on
both client and server and only communicates the changes between them,
lowering bandwith use and decreasing latency. It uses SSH for authetification,
which greatly simplifies its setup. Once the connection is established, it
communicates through encrypted UDP datagrams, which makes it more resilient to
IP address changes and connection dropouts. As you can see, it is a handy tool
to have when you are on the go. So how to get it running?

## Install

Mosh needs to be installed on both the server and the client. In most package
managers, the package is called simply `mosh`. For example, on Debian and
Ubuntu derived systems, you can install it with:

```text
$ sudo apt install mosh
```
<!-- TEASER_END -->

## Configuration

If you have already [configured SSH](https://radeksprta.eu/set-up-password-less-ssh-login-and-secure-your-ssh/),
the you just need to set up your
firewall for Mosh and you are good to go. By default, Mosh uses the first
available port in 60000-61000 range. To enable it in ufw, use:

```text
$ sudo ufw allow 60000:61000/udp
```
If you are using just a small number
of connections, it might be wiser not to open up the whole range, but only a
small part of it, say 60000-60010:

```text
$ sudo ufw allow 60000:60010/udp
```

## Usage

Th[ basic usage of Mosh is the same as SSH. So, instead of typing:

```text
$ ssh user@server
```

connect by:

```text
$ mosh user@server
```

If you are using a different port for SSH than the default 22, you have to
pass it as an option to Mosh (since it uses SSH for authetification):

```bash
$ mosh --ssh="ssh -p 1337" user@computer
```

To select the Mosh UDP port, use the `-p` switch (useful for port-forwarding):

```bash
$ mosh -p 1234 user@computer
```

Mosh will let you know when the
connection drops or when the server and client are out of sync, so it is
pleasure to use even on ropey connections. The only downside is that you
cannot scroll back through the previous program output, because Mosh
synchronizes only the visible screen state. But running tmux or screen on the
server solves that. All in all, Mosh is certainly the way to go, if you are on
an unstable connection and want to remain productive.
