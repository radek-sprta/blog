<!--
.. title: Turn Raspberry Pi Into Torrentbox
.. slug: turn-raspberry-pi-into-torrentbox
.. date: 2016-10-04 23:00:12
.. tags: download,flexget,raspberry pi,samba,server,torrent box,torrents,transmission,tutorial
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

In an earlier post I explained how to [setup a headless Raspberry Pi
server](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/). It is
sitting there, all nice and shiny. Now it is time to actually make it do
something useful. Therefore, this guide will show you how to setup your
Raspberry to work as torrent box, that automatically downloads all your
favorite legal content.

## Create download folders

First of all, you will need to an external usb drive to store the downloaded
files. You can find out how to connect one in [one of my other
posts](https://radeksprta.eu/how-to-connect-external-hard-drive-to-raspberry-pi/).
Let's say you have it mounted as `/media/external`. Then, create the
following directories for your downloads:

```text
$ mkdir /media/external/incomplete
$ mkdir /media/external/complete
```
<!-- TEASER_END -->

## Install a torrent client

Next, install a torrent client. Personally, I prefer Transmission - it is
fairly light on resources and has a good web interface. Install it by running:

```text
$ sudo apt install transmission-daemon
```

Before you can configure it, you have to stop the service
(otherwise the changes would get overwritten on restart):

```text
$ sudo service transmission-daemon stop
```

After that, open `/etc/transmission-daemon/settings.json` with nano and change
the following lines:

```bash
"download-dir": "/media/external/complete",
"encryption": 2,
"incomplete-dir": "/media/external/incomplete",
"incomplete-dir-enabled": true,
"rpc-password": "PASSWORD",
"rpc-username": "USERNAME",
"rpc-whitelist": "127.0.0.1, 192.168.*.*",
"rpc-whitelist-enabled": true,
```

Once done, save the file with `Ctrl + X`. As you can see, that's
quite a few changes. Whitelist only allows you to access the web interface
from your local network. Encryption is turned on and the rest is self-
explanatory. In order for Transmission to run and be able to write to the
external storage, you need to change the directories permissions:

```bash
$ sudo chown transmission-daemon:pi /media/external/*complete
$ chmod g+s /media/external/*complete
```

Good, you are ready to go! So, start the transmission-daemon again:

```text
$ sudo service transmission-daemon start
```

If you go to your browser and enter your Raspberry's IP
address followed by port `9091` and `/transmission/web/`
(`192.168.0.1:9091/transmission/web/` in my case), you should see
Transmission's web interface. Bravo! Now your Pi can download torrents.

## Set up network sharing

However, you really want to able to access the downloaded files from other
computers. And for that, you need Samba. So, install it by running:

```text
$ sudo apt install samba
```

To configure it, open `/etc/samba/smb.conf` in `nano` and add the following to
the end of the file:

```ini
[Downloads]
comment = Download folder
path = /media/external/complete
create mask = 0775
directory mask = 0775
read only = no
browseable = yes
public = yes
force user = pi
only guest = no
guest ok = no
```

Then save the file with `Ctrl + X`. If you want to make any
changes, check the configuration afterwards using `testparm` command. But
there is one caveat - Samba uses separate set of users and credentials.
Therefore, you have to make a Samba user `pi` and set its password. The
following command does just that:

```text
$ sudo smbpasswd -a pi
```

All set. Now, fire up your file browser, open
your network and click on your Raspberry. You should see the `Downloads`
folder right there. And if you open it and log in as `pi` with the password
you have just saved, you can see all its contents. (Probably nothing at this
point.)

## Automatic downloads with flexget

You can download the torrent contents and you can view the downloaded files
from other computers on your network. But you still have to download the
torrent files manually. If you are lazy like me and want to have the latest
release of your favorite distro automatically downloaded as soon as it is
available, you will need Flexget. It parses RSS files and passes the download
links to your favorite download manager (Transmission in this case). To ensure
that everything runs as it is supposed to, it is good idea to always run the
latest version of Flexget from Python package manager. On top of that, you
will also need the transmission-rpc package. To install and upgrade
everything, run these commands:

```text
$ sudo apt install python-pip
$ sudo pip install --upgrade setuptools
$ sudo pip install flexget transmission-rpc
```

In order to make Flexget run every hour, you simply make a systemd timer:

```bash
$ systemd-run --unit=flexget --on-active="1h" --uid=$(id -u) --gid=$(id -g) $(which flexget) execute
```

Lastly, you have to
configure Flexget. Since it is a fairly complex and powerful tool, describing
all the configuration options is out of the scope of this guide. Besides, they
have very good documentation on their
[wiki](https://flexget.com/Configuration), so no reason to double the effort.
Congratulations! You have turned your Raspberry Pi into a fully automatic
torrent box. Enjoy it and remember not to use it for any illegal activities.
But if it felt like too much work, you might want to try [Mariner torrent
searcher](https://radeksprta.eu/mariner-command-line-torrent-searcher/)
instead.
