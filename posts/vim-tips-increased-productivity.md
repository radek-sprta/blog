<!--
.. title: Vim Tips for Increased Productivity
.. slug: vim-tips-increased-productivity
.. date: 2017-11-04 13:54:06
.. tags: configuration,editor,tips,vim,vimrc
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Vim does not need to be introduced. Due to its steep learning curve, it is
often considered the bane of new Linux users. But once you get the hang of it,
there's no denying, that it's a very powerful tool. In this post I would like
to share some Vim tips, that I've learned over the years, that will improve
your productivity. It is not aimed at Vim beginners. If you are just starting
out with Vim, I recommend the excellent [Byte of
Vim](https://vim.swaroopch.com/byte-of-vim.pdf) by [Swaroop C
H](https://swaroopch.com/).

## Save file without sudo

I think we've all been in this situation - you make edits to some system
configuration and when you try to save them, you realize you don't have
sufficient privileges. No need to worry, you can save it with sudo straight
from Vim, using this command:

```bash
:w !sudo tee "%" > /dev/null
```

Since I find myself myself in such situations a lot, I
have mapped it to `:W` command in my vimrc:

```bash
$ echo 'command W w !sudo tee % > /dev/null' >> .vimrc
```
<!-- TEASER_END -->

## Paste code from clipboard

Here's another model situation. You paste code from the clipboard, but it
inevitably ends up indented off the screen. The reason for that is the ex line
editor, which runs at the heart of Vim. So Vim pastes code not as a block, but
line by line, applying the indenting rules in the process. Fixing it manually
can be quite a pain. But luckily, the indentation can be temporarily turned
off. Just enter: [terminal]:set paste[/terminal] After pasting the code, it
will get automatically unset. If you ever need to turn it of manually, just
run:

```vim
:set nopaste
```

## Comment out blocks of text

Staying with code, there are times when you need to temporarily comment out
blocks of it. Instead of commenting out line by line, you can select the whole
block in visual block mode. Switch into it using `ctrl+v` key combination.
When you have the block selected, press `I` to enter insert mode and type `#`.
Press escape and the block should get commented out. The whole sequence is as
follows:

```text
Ctrl+V
select block of code
I
#
```
To uncomment
the block again, just select the line of pound characters in visual block mode
and press x.

## Strip trailing whitespace

Trailing whitespace can often mess up your commits, so it is a good practice
to get rid of it. You can just write a simple substitute function:

```vim
func! StripTrailingWhitespace()
  exe "normal mz"
  %s/\s\\+$//ge
  exe "normal `z"
endfunc
```

And since doing things manually just leads to
forgetting them, run it automatically when saving a file. Add the following
snippet to your vimrc:

```vim
autocmd BufWrite :call StripTrailingWhitespace()
```

## Word and character count

While Vim does not show character or word count in status line, it is easy
enough to find it out. Just press `g` followed by `ctrl+g`. All the
information will be displayed in the status line.

## Insert newline without entering insert mode

When formatting files, I often want to add a blank line. However, doing it
with `o` and `O` enters insert mode, which I have to promptly escape from.
Therefore, I have added a mappings `[O` and `]o` to add newline while staying
in normal mode. You can add them to vimrc like that or change them to any key
combination you prefer.

```text
nmap [O O
nmap ]o o
```

## Escape normal mode

I don't know about you, by I am not fond of stretching my hands all the way to
press Esc. You can avoid hand sore by mapping it to some key combination, that
is not frequently used. I prefer `jj`. Just add this to your vimrc:

```text
inoremap jj
```

## Conclusion

These are some of the tricks that I use daily. My vimrc is publicly available
in my [dotfiles repository](https://gitlab.com/radek-sprta/dotfiles), so you
can check it out. Also, see my post about [managing your Vim
plugins](https://radeksprta.eu/managing-vim-plugins-easily-using-git/). And if
you have any Vim tips to share, leave a comment below the article.
