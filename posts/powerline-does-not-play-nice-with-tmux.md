<!--
.. title: Powerline does not play nice with Tmux
.. slug: powerline-does-not-play-nice-with-tmux
.. date: 2023-06-10 20:13:25 UTC+02:00
.. tags: tmux, powerline
.. category: Linux
.. link: 
.. description: While attempting to integrate Powerline into Tmux status line, I encountered compatibility issues and high CPU usage, leading me to look for an alternative solution.
.. type: text
-->

Tmux has been an invaluable tool for me over the years. If you haven't come across it before, Tmux is akin to a window manager for your terminal. It allows you to create multiple tabs, split windows into panes, and, most notably, keeps your remote sessions alive even after you disconnect.

I am also a fan of [Powerline](https://github.com/powerline/powerline), the fancy status line for Vim, shell prompt and other applications. So, I naturally wanted to use it for my Tmux status line.  But to my great surprise, it did not work as expected.
<!-- TEASER_END -->

First of all, the Tmux plugin for saving sessions ([tmux-continuum](https://github.com/tmux-plugins/tmux-continuum)) has stopped working. It appears that Powerline overrides the hook placed in Tmux's right status bar by tmux-continuum.

Moreover, I started seeing huge CPU spikes at random intervals, because Powerline was going crazy. It seems there is a [bug in Powerline causing these issues](https://github.com/tmux/tmux/issues/361).

Disappointed, I looked for an alternative and discovered [tmux-powerline](https://github.com/erikw/tmux-powerline). It does exactly what I need without any adverse effects. I recommend giving it a try as well.
