<!--
.. title: Postfix SASL authentication in Alpine Linux
.. slug: postfix-sasl-authentication-in-alpine-linux
.. date: 2022-01-24 08:28:57 UTC+01:00
.. tags: postfix, sasl, smtp, authentication, security, alpine
.. category: Linux
.. link: 
.. description: Learn how to set up SASL authentication for Postfix on Alpine Linux.
.. type: text
-->

Most of the popular Postfix Docker images assume that you run the service as a local SMTP forwarder. Therefore, they do not bother with authentication. So, if you want to use Postfix as your central mail sending agent, you need to roll your own. This post will walk you through the setup of Postfix with SASL authentication on Alpine Linux, my container distro of choice.
<!-- TEASER_END -->

First of all, you need to get all the necessary packages. This means Postfix itself and Cyrus SASL libraries, which are used for authentication:

```sh
apk add cyrus-sasl cyrus-sasl-crammd5 cyrus-sasl-digestmd5 cyrus-sasl-login cyrus-sasl-ntlm postfix
```
The next step is to configure the authentication daemon. Create `/etc/sasl2/smtpd.conf` configuration file with the following contents:

```text
pwcheck_method: auxprop
auxprop_plugin: sasldb
mech_list: PLAIN LOGIN CRAM-MD5 DIGEST-MD5 NTLM
```

This does 2 things. Firstly, it enables the more advanced authentication methods, such as CRAM-MD5 and DIGEST-MD5. Secondly, it chooses sasldb as the backend for storing hashed credentials.

So far, so good. Now we need to actually store some credentials:

```sh
echo "${SMTP_PASSWORD}" | saslpasswd2 -p -c -u "${SMTP_DOMAIN}" "${SMTP_USER}"
```

And run the SASL daemon. If you want to try it manually, run:

```sh
/usr/sbin/saslauthd -a sasldb -c -d
```

In order to check that it's working as expected, use `testsaslauthd`:

```sh
testsaslauthd -u "${SMTP_USER}" -r "${SMTP_DOMAIN}" -p "${SMTP_PASSWORD}" -s smtpd
```

On SASL side, we are done. Now you need to configure several options in `/etc/postfix/main.cf` (or at runtime using `postfix -e`). Namely these:

```ini
smtpd_sasl_auth_enable=yes
broken_sasl_auth_clients=yes
smtpd_sasl_security_options=noanonymous,noplaintext
smtpd_sasl_tls_security_options=noanonymous
smtpd_relay_restrictions=permit_sasl_authenticated,reject_unauth_destination
```

This enables SASL, allows clients that do not respect RFC, disables plaintext authentication methods over unencrypted connections and, last but not least, disables sending by unauthenticated clients.

You can test the whole setup via telnet. The lines not starting with a number are the ones you should type in:

```text
telnet "${POSTFIX_MACHINE}" 25
EHLO test
...
250-AUTH=DIGEST-MD5 CRAM-MD5
AUTH CRAM-MD5
334 CfZM9XhGF7KHhMPdrnRJLWjlZudSEmKAUaoB6EcARVY01At0eKHUX9Z0fohKY==
```

Now, the last line is the login challenge. To answer it, run the following command:

```text
gen-auth CRAM-MD5 ${SMTP_USER}@${SMTP_DOMAIN} "${SMTP_PASSWORD}"
challenge: CfZM9XhGF7KHhMPdrnRJLWjlZudSEmKAUaoB6EcARVY01At0eKHUX9Z0fohKY==
Udp+H+1OAlrxr1b+TuOl5GL5Hpiygy6z2QvFIWIwB/BkUlrKAqss+zJPEOZn9lZWPWelO9kle5qjOOiWTowi3Q==
```

Then just paste the result to telnet and you should see:

`235 2.7.0 Authentication successful`

That's it. I bet it was easier than you expected. With this, you are ready to configure SASL on your own image. But if you don't feel like doing that, you can just go ahead and use [rsprta/postfix](https://hub.docker.com/r/rsprta/postfix).
