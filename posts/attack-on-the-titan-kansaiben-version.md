<!--
.. title: Attack on the Titan - Kansaiben version
.. slug: attack-on-the-titan-kansaiben-version
.. date: 2015-05-10 07:15:02
.. tags: attack on the titan,kansaiben,manga
.. category: Japanese
.. link: 
.. description: 
.. type: text
.. categories: Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

Last year, the Attack on the Titan attraction has opened in Universal Studios
Japan in Ôsaka. It featured 15 m statues of Eren and the Female Titan.
Naturally, it was an immense success. Now, to celebrate one year anniversary
of its opening, the publishing house has released Kansaiben version of the
first manga.

![Source: arecollection.hatenablog.com](https://cdn-ak.f.st-hatena.com/images/fotolife/o/ornith/20150313/20150313012944.jpg)
<!-- TEASER_END -->

It is quite hilarious, as you can hopefully see on the pictures. In addition
to being completely in Kansaiben, a lot of the names and facts were changed to
fit the Kansai setting. Thus, titans (kyozin) became huge dudes (dekai ossan).
Walls Maria and Rose became Nanba and Umeda (downtowns of Ôsaka) and so on. I
recommend reading it while you can. And if you do not know any Kansaiben, it
is a great opportunity to learn some! ;) ~~You can find the
manga [here](http://shingeki.net/kansai/ "Attack on the Titan - Kansaiben
version").~~

## Update

Unfortunately, it didn't last long. Hopefully, they will make it available
again in the future. Meanwhile, you can [switch Facebook to
Kansaiben](https://radeksprta.eu/facebook-in-kansaiben/) instead.

