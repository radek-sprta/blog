<!--
.. title: Run MiniDLNA on TrueNAS Scale
.. slug: run-minidlna-on-truenas-scale
.. date: 2025-01-13 01:10:22 UTC+01:00
.. tags: truenas scale, minidlna, dlna, media server, jellyfin
.. category: Linux
.. link:
.. description:
.. type: text
-->

I have recently set up a Jellyfin media server on my TrueNAS Scale box. It is an
excellent piece of software and I am very happy with it overall. However, its
DLNA plugin does not seem to play nicely with my TV. So I turned to my favorite
DLNA server, MiniDLNA.

Unfortunately, MiniDLNA is not available in TrueNAS Scale's App catalog. That
said, TrueNAS Scale uses k3s under the hood to run apps, so you can easily deploy
your own containerized applications with ease.
<!-- TEASER_END -->

# Configuration

Navigate to **Apps** -> **Discover Apps** and then click the **Custom Apps**
button. A configuration form for the new app will appear. Fill it out as follows:

```yaml
Application Name: minidlna
Image Repository: vladgh/minidlna
Container Environment Variables:
    MINIDLNA_FRIENDLY_NAME: My media
    MINIDLNA_INOTIFY: yes
    MINIDLNA_MEDIA_DIR: /media
Networking: use Host network
Host Path Volumes:
  - Host Path: /mnt/tank/media
    Mount Path: /media
```

Couple notes about the configuration:

- I use `vladgh/minidlna` image, because it is well maintained.
- The `MINIDLNA_FRIENDLY_NAME` can be set to your preferred name.
- Inotify ensures MiniDLNA automatically recognizes newly added files.
- Host networking is required for MiniDLNA to broadcast itself on your local network.
- Set the host path to the location of you media files on TrueNAS.
- You can configure resource limits based on your hardware, but MiniDLNA is not
resource intensive.

# Conclusion

As shown above, running custom apps on TrueNAS Scale is straightforward. You can
apply a similar approach for deploying other containerized applications. This
flexibility makes TrueNAS Scale a powerful solution for managing your home server
needs.
