<!--
.. title: Fix Steam on Ubuntu Without Hassle
.. slug: fix-steam-ubuntu-without-hassle
.. date: 2016-08-16 23:25:46
.. tags: bash,fix,script,steam
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

While greatly improved since the beta days, Steam on Linux is not perfect yet.
After updates, it is often neccessary to fix Steam. You might be familiar with
the following error:

```text
SteamUpdateUI: An X Error occurred
X Error of failed request: BadValue (integer parameter out of range for operation)
```

The problem is caused by the Steam's bundled libraries.
But luckily, the solution is straightforward. All you need to do is to remove
them. And to make your life easier, you can make a simple script to handle
that. Let's call it steamfix.sh:

```bash
#!/usr/bin/env bash
set -exu
cd $HOME/.steam/ubuntu12_32/steam-runtime/i386/usr/lib/i386-linux-gnu
mv libstdc++.so.6 libstdc++.so.6.bak
cd $HOME/.steam/ubuntu12_32/steam-runtime/amd64/usr/lib/x86_64-linux-gnu
mv libstdc++.so.6 libstdc++.so.6.bak
```

Don't forget to make it executable with `chmod
+x` and put it into `/usr/local/bin`. Now every time your Steam stops running
after an update, executing `steamfix.sh` will make everything alright. Do you
also have a Steam controller? If so, check out [how to configure it on
Linux](https://radeksprta.eu/using-steam-controller-in-ubuntu/).

