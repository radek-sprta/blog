<!--
.. title: Scarlet Johansson in Ghost in the Shell
.. slug: scarlet-johansson-in-ghost-in-the-shell
.. date: 2016-04-25 07:52:59
.. tags: anime,controversy,ghost in the shell,movie,remake,scarlett johansson
.. category: Japanese
.. link: 
.. description: 
.. type: text
.. categories: Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

![Scarlett Johansson in Ghost in the Shell as Major Motoko Kusanagi](https://pbs.twimg.com/media/CgBITcbUAAEdMiV.png)

Couple days ago, the media confirmed, that [Scarlet Johansson will star as
Major Motoko Kusanagi](https://fandom.wikia.com/articles/first-look-scarlett-johansson-ghost-shell)
in the upcoming Hollywood adaptation of the cyberpunk
classic Ghost in the Shell. This immediately caused a huge uproar, with posts
accusing the creators of 'white-washing' [springing up like
mushrooms](https://www.theguardian.com/film/2016/apr/15/scarlett-johanssons-role-in-ghost-in-the-shell-ignites-twitter-storm).
There is even a [petition that supports casting of an Asian actress in favor of Scarlett
Johansson](https://www.thepetitionsite.com/683/366/733/dreamworks-dont-whitewash-japanese-films/).
To be honest, I was baffled when I found out about
the backlash. But more about that later. The opponents of have two issues with
Johansson's. Firstly, that the creators chose a white female for the role of a
Japanese woman. Secondly, that they refer to the character as 'The Major' to
further obscure its Japanese background. Let's take a look at the legitimacy
of those claims. Motoko Kusanagi uses an artificial, robotic body. So her
ethnicity has no impact on her looks. In the original movie she is pictured
with blue eyes and distinctly Caucasian features. I would argue that casting a
white female to the role is actually staying true to the original. You can see
the picture below and decide for yourself. Does the Major strike you as
ethically Japanese in the anime?
<!-- TEASER_END -->

![Motoko Kusanagi in the Ghost in the Shell anime](https://comicvine.gamespot.com/a/uploads/original/11/114265/2241826-motoko.jpg)

As for calling the character simply 'The Major'. The complaints about this are
even more unfounded. Throughout the original movie, Motoko Kusanagi is
referred to as 'Major'. In fact, there is only one character that calls her by
her name, Batô. Therefore, it is only natural to call her the same way as in
the anime.

## Conclusion

In conclusion, there is nothing wrong with the casting of Scarlett Johansson
as 'The Major'. This is no case of 'white-washing', because the character is
not supposed to look Asian in the first place. In fact, due to her resemblance
to the anime version and proven athletic ability (Avengers, Captain America),
she might be the ideal candidate. Case closed.

