<!--
.. title: Download Flickr Images Easily Using Script
.. slug: easily-download-flickr-images
.. date: 2016-09-03 23:30:33
.. tags: command line,download,pictures,python,script,wallpaper
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

I like to change my wallpaper to reflect the current season. Since I spend
large parts of the day in front of computer, this gives me a glimpse of the
outside world. My favorite source of high quality photos has for the longest
time been [Flickr](https://www.flickr.com).  There is one thing though, that
has always bothered me on Flickr. Some authors do not allow download of their
photos. I don't see the point doing of this. If someone has the picture open
in their browser, it is already stored stored on their computer. Therefore,
they can just copy it from the cache. Or look for link to the picture in the
source code. What I'm trying to say is, that if someone wants to download the
picture, they will. Don't upload the picture to the Internet in the first
place, if you don't like other people having it. I tend to download a lot of
wallpapers at once. Because of that, digging through the source code can get a
bit tedious. To deal with that, I wrote a simple [script to download Flickr
images](https://gitlab.com/radek-sprta/flickr-downloader) in Python to do the
heavy-lifting for me. You can use it with the URLs as arguments or saved in a
text file, one per line. By default, it downloads the original image, but you
can specify the size. Enjoy!

