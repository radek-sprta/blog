<!--
.. title: Protect Your Eyes From Computer Eye Strain
.. slug: protect-eyes-computer-eye-strain
.. date: 2017-02-09 14:34:13
.. tags: eye yoga,eyes,insomnia,melatonin disruption,redshift,safeeyes
.. category: Health
.. link: 
.. description: 
.. type: text
.. categories: Health,Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Internet has revolutionized our lives. It has changed the way we work,
communicate and access information. But as we spend ever more time in front of
shining computer and phone screens, our eyes pay the price. We are [plagued
with near-vision, ](https://www.marksdailyapple.com/the-modern-assault-on-eyesight-health/) 
eye strain and other problems. Allow me to introduce two
tools, that can help in relieving the computer eye strain.

## Eye excercizes with SafeEyes

We typically watch screens from very close distances, trying to read the tiny
letters. [That is the biggest culprit behind computer eye
strain](https://chriskresser.com/is-too-much-internet-use-making-us-sick/),
because our eyes have simply not evolved for that. So, a collection of
techniques called eye yoga were developed to battle eye fatigue. But as with
anything, it is easy to forget about them, while you are concentrating on
other stuff. Fear naught, [SafeEyes](https://github.com/slgobinath/SafeEyes)
comes to save the day. It's a simple app, that sits in the taskbar and every
once in a while reminds you to take a break, suggesting an eye yoga exercise.
Kind of like EyeLeo for Mac. It is intelligent enough not to pop up while you
are watching a movie. And you can skip a break, if you need to, so it is
fairly unobtrusive.
<!-- TEASER_END -->

## Filter out blue light with Redshift

The other application I would like to talk about is
[Redshift](http://jonls.dk/redshift/). It shifts the hue of your screen
towards the red part of the spectrum, based on the time of the day and your
location. Why would you want to do that? See, our bodies follow the circadian
rhythm. When the sun is up, we are energized. When it goes down, we get
sleepy. [Turns out the blue light emitting from our screens inhibits the
production of melatonin, disrupting our 
sleep](https://chriskresser.com/how-artificial-light-is-wrecking-your-sleep-and-what-to-do-about-it/).
We fall asleep later, have lower sleep quality and cannot get up in the morning. So
use Redshift today and see the difference tomorrow. And don't worry, your eyes
will adjust to the red tint, so you won't feel like you are watching Blood
Rage.

## Conclusion

While these two apps can reduce the negative effects of too much screen time,
it should go without saying, that to really protect your vision, their use has
to go hand in hand with a healthy diet and active lifestyle. Also, check out
how to [automatically turn off LEDs on Raspberry
Pi](https://radeksprta.eu/automatically-turn-off-the-leds-on-raspberry-pi/),
for that undisrupted sleep experience.
