<!--
.. title: Loop Over Dictionary Attribute in Ansible
.. slug: loop-over-dictionary-attribute-in-ansible
.. date: 2020-07-04 10:13:50 UTC+02:00
.. tags: ansible,automatization,filter
.. category: DevOps
.. link: 
.. description: Sometimes, you might want to loop over a certain dictionary attribute in Ansible. Learn how to do it in this post.
.. type: text
-->

Working with variables can sometimes get tricky in Ansible. Say, you have a dictionary where you want loop over a certain attribute, not all values. For example, when your variables are declared like this:

```yaml
interfaces:
  eth0:
    ip: 10.0.0.10
    mask: 16
  eth1:
    ip: 192.168.1.100
    mask: 24
```

How do you just loop over all the IP addresses? This is where the map filter comes in:

```yaml
vars:
  ip_addresses: "{{ interfaces.values() | map(attribute='container') | list }}"
```

The snippet takes all values from the `interfaces` dictionary (`eth0`, `eth1`) and then extracts the `ip` attribute from them. Finally, it casts the result into list.

[Ansible filters](https://docs.ansible.com/ansible/latest/user_guide/playbooks_filters.html) are a very powerful tool, so I would recommend you take some time to read it thoroughly. If you are looking for more Ansible tips, such as [how to make a playbook distribution agnostic](https://radeksprta.eu/posts/make-ansible-playbook-distribution-agnostic) or [setup your laptop with Ansible](https://radeksprta.eu/posts/automatically-setup-computer-ansible-playbook), look [here](https://radeksprta.eu/categories/ansible/).
