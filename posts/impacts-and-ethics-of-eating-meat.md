<!--
.. title: Impacts and Ethics of Eating Meat
.. slug: impacts-and-ethics-of-eating-meat
.. date: 2016-11-08 19:50:48
.. tags: environment,podcast,Link,red meat,vegetarian and vegan diets
.. category: Health
.. link: 
.. description: 
.. type: text
.. categories: Health
.. has_math: no
.. status: published
.. wp-status: publish
-->

The health benefits of vegan diet over a balanced omnivorous one have been
largely disproved by nutritional research. Since then, the argument has
shifted to the negative environmental impact of producing meat. Recently,
media has been swarming with articles such as [Mean is
horrible](https://www.washingtonpost.com/news/wonk/wp/2016/06/30/how-meat-is-destroying-the-planet-in-seven-charts/)
on Washington Post, or [Tax meat and
dairy to cut emissions and save lives, study
urges](https://www.theguardian.com/environment/2016/nov/07/tax-meat-and-dairy-to-cut-emissions-and-save-lives-study-urges)
on The Guardian. Their authors
cherry-pick arguments to support their position, that eating meat (especially
beef) is detrimental to the environment. Meanwhile, they ignore any downsides
of growing crops.  On the latest episode of Revolution Health Radio, Diana
Rogers presents a more complete picture of the situation. In the process, she
dispels the notion that growing crops is the only environmentally and
ethically feasible way of producing food. Listen to it here:

[Impacts and Ethics of Eating Meat](
https://chriskresser.com/impacts-and-ethics-of-eating-meat-with-diana-rodgers/)
