<!--
.. title: Non-capturing groups in regular expressions
.. slug: non-capturing-groups-in-regular-expressions
.. date: 2024-01-06 23:36:40 UTC+01:00
.. tags: TIL, regex
.. category: Linux
.. link: 
.. description: Recently, I learned about the utility of non-capturing groups in regular expressions. See here how to use them.
.. type: text
-->

In one of my recent merge requests, someone suggested to use non-capturing
group to simplify one of my regular expressions. I haven't heard of those before,
so I hit up the search bar.

Turns out, non-capturing groups use this syntax `(?:foobar)`. And unlike normal groups,
they donot store reference to the matched content. That makes them perfect when you
just want to specify a quantifier for a group.

I am definitely going to start using them often now. It's moments like these, that make
me love code reviews. No matter how much you think know, there is always someone
dropping knowledge bombs.

For more regular expression tips, check out my
[lookbehind assertions example](/posts/one-liner-to-download-latest-phpmyadmin) and
[Mozilla's Regular Expressions Cheet Sheet](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Regular_Expressions/Cheatsheet).
