<!--
.. title: Cube and Me - A Fresh Breath in Rogue-likes
.. slug: cube-and-me
.. date: 2014-12-12 16:04:00
.. tags: kickstarter,rpg
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games
.. has_math: no
.. status: published
.. wp-status: publish
-->

Lately, when I check gaming news, I rarely see something unique I could get
excited about. Survival sandbox game no. 2443, another Assassin's Creed, the
same old stuff. However, on Kickstarter, you can sometimes find hidden jewels.
And Cube and Me is one of them. The gameplay is something I would best
describe as tamagotchi rogue-like with the battle system similar to Marvel vs
Capcom. And yes, it is every bit as awesome as it sounds.

{{% media url="https://www.youtube.com/watch?v=FTM73SBnZ8s" %}}

In the opening scene, you find a
crashed space vessel. And inside it, strange alien lifeforms -- Cubes. You
take them home, because who needs cats and dogs when you have Cubes as your
pets, right? Your house constitutes one half of the gameplay. Inside there,
you take care of your Cubes. In order to keep them happy, you have to wash
them, feed them and so on. You can equip your house with TV, sofa, pictures
and other furniture to make their life even more comfortable. And where do you
get money for all of that? Well, you make living by folding envelopes. Yea,
that's right. As weird as it sounds, it actually makes for a fun mini-game and
becoming an envelope-folding virtuoso takes quite the skill.

Just as you
settle down a bit, you see the news about another spaceship crashing near
California. Naturally, you decide to check it out and learn more about these
mysterious creatures. Here comes the other part of the game, dungeoneering. If
you have ever played a roguelike, you will be right at home. There are
randomly generated rooms with monsters, treasures and all that jazz.

But what
sets Cube and Me apart is the fighting system. When I say Marvel vs Capcom, I
mean the craziness of dodging gazillion projectiles and enemies coming at you
from all directions, blasting them with your hypers and calling for assists to
light up the screen. It may not be as mechanically intensive, but it makes up
for it with careful resource management. You can hold the Cubes with your
mouse to move them around and enter hyper mode. But if you hold it for too
long, you the cubes will overheat, leaving them at the mercy of your enemies.
Which they will not show much of. In addition, you can leave your Cubes on
elevated platforms to get some breathing room. But again, leave them there for
too long and the platforms will crumble. And then, there are the temporary
boosts. It is crazy, it is fun, and I assure you, you will love it.

## Conclusion

The detailed and cute 2D pixel graphics is accompanied by an 8-bit soundtrack.
Combine that with the slightly far-fetched sci-fi setting and you get a game
that will take you right back into the SNES days. Cube and Me is currently in
alpha state, with demo available. So, go ahead and try it. If you like it, do not forget support
the game on [Kickstarter](https://www.kickstarter.com/projects/797210197/cube-and-me-relaunch "Cube and Me Kickstarter page"),
so it sees the light of the day.
