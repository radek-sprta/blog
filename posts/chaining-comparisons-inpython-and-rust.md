<!--
.. title: Chaining Comparisons in Python and Rust
.. slug: chaining-comparisons-in-python-and-rust
.. date: 2023-10-08 08:49:23 UTC+02:00
.. tags: TIL, syntax
.. category: Rust
.. link: 
.. description: Learn how alternative approaches to Python's comparison chaining in Rust.
.. type: text
-->

I love chaining comparisons in Python. Not only are expressions like `a < b < c` terser,
but I also find them more readable than the alternative (`a < b and b < c`). So when I 
started coding in Rust, I wanted something similar.

## Limitation in Rust

Unfortunately, Rust does not support chaining operators. The arguments are revolving around 
cases where the interpretation would not be obvious (`a != b != c`), or situations where they
they could potentially decrease readability when used incorrectly. Those are fair points.
But luckily, there are alternative approaches to achieve the same goals in Rust.

<!-- TEASER_END -->

## Alternative approaches

**Python: `a < b < c` vs. Rust: `(a..b).contains(c)`**

In Rust, you can achieve a similar result to Python's `a < b < c` using the `contains()` method with a range:

```rust
let c_in_range = (a..b).contains(&c);
```

**Python: `a < b <= c` vs. Rust: `(a..=b).contains(c)`**

Similarly, for comparisons like `a < b <= c`, you can use the inclusive range operator in Rust and the `contains()` method:

```rust
let c_in_range = (a..=b).contains(&c);
```

This approach maintains readability and provides a Rust equivalent to Python's chaining comparisons.

## Looking forward

In the future, when `is_sorted()` method for iterators makes it to mainline Rust, it will bring even
more possibilities for chained comparisons:

**Python: `a <= b <= c` vs. Rust: `[a, b, c].is_sorted()`**

## Conclusion

While Rust may lack direct support for chaining comparisons, it offers alternative approaches to maintain code readability and expressiveness. These make coding in Rust just as enjoyable and concise as in Python, once you get the hang of them.
