<!--
.. title: NHK Radio Special on Tôhoku Earthquake
.. slug: nhk-radio-special-tohoku-earthquake
.. date: 2015-03-11 23:54:35
.. tags: japan,tohoku earthquake
.. category: Japanese
.. link: 
.. description: 
.. type: text
.. categories: Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

It has been 4 years since the Tôhoku Earthquake already. This tragedy has
extinguished countless lives, but among the continuing problems at Hukusima
Power Plant and reconstruction work delays, many people are still suffering
from the ruin it has brought. In reaction, NHK Radio have released a 3 part
special, where they revisit these areas to look at the current situation and
speak with the locals about the disaster and the future.  It is quite
interesting. Unfortunately, NHK delete the podcasts from the feed after 2
days. So I have decided to upload them on Youtube:

## Part 1

{{% media url="https://www.youtube.com/watch?v=ye8PO0LBDRU" %}}

## Part 2

{{% media url="https://www.youtube.com/watch?v=bForkfOKvK8" %}}

## Part 3

{{% media url="https://www.youtube.com/watch?v=hGPTZ33Ijio" %}}
