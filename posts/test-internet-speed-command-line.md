<!--
.. title: Test Internet Speed in Command Line
.. slug: test-internet-speed-command-line
.. date: 2015-07-16 14:54:45
.. tags: command line,internet
.. category: Linux
.. link:
.. description:
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Sometimes the Internet is sluggish and you need to find out if your connection
is the culprit. The usual routine to find out the connection speed is the
following. First, switch to your browser. Then type out the address of one of
the speed testing sites. Finally, navigate through, usually bloated and
annoying, GUI. For those who cannot be bothered to do that (and are on \*nix
system), I have a simple trick to test internet speed in command line.  You
can just create an alias called `speedtest`. If you use bash, then running
this command will do:

```bash
$ echo "alias speedtest='wget https://cachefly.cachefly.net/100mb.test -O /dev/null'" >> .bash_aliases
```

However, my personal favorite is
[fish](https://fishshell.com/) (the shell, not the animal). In fish, you can
make custom function in `~/.config/fish/functions/speedtest.fish`. The code is
below:
<!-- TEASER_END -->

```fish
function speedtest --description "Test the network speed"
  if test "$argv" = "10"
    wget -O /dev/null https://cachefly.cachefly.net/10mb.test
  else
    wget -O /dev/null https://cachefly.cachefly.net/100mb.test
  end
end
```

In order to measure your connection speed, just type
`speedtest` in the command line:

```text
$ speedtest 100mb.test
100%[=====================================>] 100.00M 3.56MB/s Time 25s
```

Wget and bash are usually installed by default, so you should
have no trouble running the alias. As for slower connections, use `10mb.test`
instead of `100mb.test`. You can find more useful tricks in the [command-line
category](https://radeksprta.eu/categories/command-line/). Also check my [Gitlab
page](https://gitlab.com/radek-sprta).
