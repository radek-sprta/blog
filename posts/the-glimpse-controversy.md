<!--
.. title: The Glimpse Controversy
.. slug: the-glimpse-controversy
.. date: 2020-01-23 20:39:15
.. tags: controversy,editor,gimp,glimpse,graphics
.. category: Essay
.. link: 
.. description: 
.. type: text
.. categories: Essay,Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

The first version of Glimpse photo editor, a
fork of Gimp, just came out. Unfortunately, an avalanche of criticism by some
community members has promptly followed. The reactions range from claiming the
Glimpse team are not bringing any value, to accusing them of stealing the Gimp
code. To be honest, I found the overall tone quite unreasonable. So, I will
try to refute the claims and explain why forking Gimp into Glimpse is a
positive development.

## "The authors are over-sensitive SJWs"

The most common critique is, that the project was started because of personal
sensitivities. More specifically, because the team found "gimp" to be an
offensive term. However, if you see [their motivation behind the
rebranding](https://web.archive.org/web/20200625073643/https://glimpse-editor.github.io/about/),
you will learn, that it was not the case. Rather, the reason was, that
the name hampered Gimp's adoption in professional and education settings.
Therefore, to further spread open-source software, a change was necessary.
Since the Gimp team had no intention of rebranding it, they [encouraged
forking the project to rename
it](https://www.gimp.org/docs/userfaq.html#i-dont-like-the-name-gimp-will-you-change-it)
themselves.
<!-- TEASER_END -->

## "It's just a name change"

Another prevalent complain is, that Glimpse brings nothing beyond a name
change. Well, anyone expecting sweeping changes in 0.1 version needs a reality
check. HOwerver, the team already introduced some nice improvements,
such as new Windows installer and improved builds. And their ultimate
goal is to develop a better GUI (another thing that original
authors refuse to change). But you have to start somewhere. Release early,
release often, as the say.

## "How can they ask for money, without a single line of code?"

Some people were outraged by the fact, that the Glimpse team had a Patreon
page. The argument was, that they are charging money for other programmers'
work. First of all, the donations are not supposed to be thanks for the code
already written. Instead, they are meant to fund the future work outlined
above. Furthermore, they actually [share the funds with
Gimp](https://opencollective.com/glimpse/expenses/10167). Compare that to
Gimp, who have failed to create an organization you could donate to. As a
result, you have to hunt down the individual developers on Patreon. It is
plain to see, that the Glimpse model simplifies things greatly.

## "They haven't touched Gimp before"

Critics have also pointed out, that the Glimpse team members have never
contributed to Gimp before. But that actually means, that the project has
attracted new contributors. People, who might have chosen not to contribute to
Gimp, due to difference of opinion, have found a venue for implementing their
ideas. Open-source is always coping with a lack of new developers, so this is
definitely a positive result.

## Conclusion

The creation of Glimpse has given an opportunity to people, who might not
contribute to open-source otherwise. They provide a alternative solution to
things, that Gimp are not willing to change. All the while trying to foster a
positive relationship with the upstream project. One, where code and funds are
shared for the benefit of both. That's something to applaud, not scorn.
