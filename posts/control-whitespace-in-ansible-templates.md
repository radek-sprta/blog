<!--
.. title: Control Whitespace in Ansible Templates
.. slug: control-whitespace-in-ansible-templates
.. date: 2021-01-02 12:13:38 UTC+01:00
.. tags: ansible, whitespace, template, jinja
.. category: DevOps
.. link: 
.. description: The default whitespace handling in Ansible templates can be cumbersome. So, learn how automatically strip out leading whitespace.
.. type: text
-->

Ansible uses the powerful Jinja templating engine. However, the way it handles whitespace in templates is not ideal. Specifically, the fact that it preserves leading whitespace around Jinja blocks. So, you can either not indent Jinja syntax, making the templates hard to comprehend, or accept broken indentation in the resulting file (not an option with whitespace-sensitive formats such as yaml).

Luckily, there is a third option. Jinja has two configuration options regarding whitespace:
<!-- TEASER_END -->

* `trim_blocks` - first newline after template tag is removed automatically
* `lstrip_blocks` - strip tabs and spaces from beginning of the line to the start of a block

While `trim_blocks` has been on by default since Ansible 2.4, that is not the case with `lstrip_blocks`. In order to turn it on, you need to put the following line at the very top of your template:

`#jinja2: lstrip_blocks: "True"`

## Comparison

Let's take the following template:

```jinja
{% set _dataset = datasets[dataset] %}
[{{ dataset }}]
    {% for key, value in _dataset.items() %}
    {{ key }} = {{ value }}
    {% endfor %}
```

without `lstrip_blocks`, the result would look like this:

```toml
[home]
        recursive = yes
    use_template = default
```

Now, with `lstrip_blocks` on, you get correct indentation:

```toml
[home]
    recursive = yes
    use_template = default
```

Naturally, you can locally overwrite the configuration by adding `+` to the Jinja block:

```jinja
    {%+ if foo %}Preserve indentation{% endif %}
```

## Conclusion

This configuration option was a lifesaver for me. Hopefully, it will help you make your Ansible templates more readable as well. You can find more examples in the [Jinja documentation](https://jinja.palletsprojects.com/en/latest/templates/#whitespace-control).
