<!--
.. title: Facebook in Kansaiben - Finally Here!
.. slug: facebook-in-kansaiben
.. date: 2014-10-12 12:23:35
.. tags: facebook,kansaiben
.. category: Japanese
.. link: 
.. description: 
.. type: text
.. categories: Japanese
.. has_math: no
.. status: published
.. wp-status: publish
-->

On 8th October, Facebook has rolled out a feature we have all been waiting for
- Kansaiben version. (Everyone is a Kansaiben lover, some people just don't
know it yet ;). Here is the release statement in Kansaiben:

![Facebook in Kansaiben, source: itmedia.co.jp](https://image.itmedia.co.jp/nl/articles/1410/08/tomomi_141008facebook02.png)

For example, like has become ええやん！, comment つっこむ and share わけわけ, along with
bunch of other stuff. So head straight into your language settings and give it
a try! And if you also like Attack on the Titan, there is [Kansaiben version
of that](https://radeksprta.eu/attack-on-the-titan-kansaiben-version/) too!

