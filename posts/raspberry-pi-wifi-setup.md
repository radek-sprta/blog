<!--
.. title: Raspberry Pi Wifi Setup in Couple Steps
.. slug: raspberry-pi-wifi-setup
.. date: 2017-06-26 16:01:37
.. tags: network,raspbian,server,tutorial,wifi
.. category: Raspberry Pi
.. link:
.. description:
.. type: text
.. categories: Raspberry Pi
.. has_math: no
.. status: published
.. wp-status: publish
-->

Say, you might have finished [configuring your Raspberry
Pi](https://radeksprta.eu/setting-up-headless-raspberry-pi-server/). It is
running smoothly, but there one thing that gets on your nerves. The pesky
ethernet cable is getting in your way. Well, it's time to get rid of it for
good and setup the wifi. To do that, you need to edit the following two files:
`/etc/network/interfaces` and `/etc/wpa_supplicant/wpa_supplicant.conf`.

## Configure the wireless

Let's start with the network intefaces. Open the first file in text editor:


```text
$ sudo nano /etc/network/interfaces
```

Go to the `wlan0` section, which stands for
you wifi adapter, and edit it so it looks like this:

```text
allow-hotplug wlan0
iface wlan0 inet dhcp
  wpa-conf /etc/wpa_supplicant/wpa_supplicant.conf
iface default inet dhcp
```

In case your `/etc/network/interfaces` does
not include a `wlan0` section, just copy the whole thing. When you are done,
press `ctrl + x` to save the changes.
<!-- TEASER_END -->

## Connect to your WLAN

Now all remains to do, is to edit your wifi settings. Edit the
`wpa_supplicant.conf` using the following command:

```text
$ sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
```

If your network uses the WPA/WPA2 for encryption, add the following section:

```text
network={
  ssid="Name of your network"
  psk="Password"
  key_mgmt=WPA-PSK
}
```

But if you are using WEP (you really should be using the more secure WPA2 by the
way), add this instead:

```text
network={
  ssid="Name of your network"
  wep_key0="Password"
  key_mgmt=NONE
}
```

Fill in your network's name and
password accordingy and save the changes by pressing `ctrl + x`. That's it.
You can test the changes by rebooting the Pi with the ethernet cable
disconnected. Enjoy your new found freedom from cables.
