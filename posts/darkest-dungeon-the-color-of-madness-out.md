<!--
.. title: Darkest Dungeon: The Color of Madness Is out
.. slug: darkest-dungeon-the-color-of-madness-out
.. date: 2018-06-23 19:00:53
.. tags: darkest dungeon,dlc,rpg
.. category: Games
.. link: 
.. description: 
.. type: text
.. categories: Games
.. has_math: no
.. status: published
.. wp-status: publish
-->

[Red Hook Studios](https://www.darkestdungeon.com/) have just released Darkest
Dungeon: The Color of Madness. It's the latest DLC for the critically
acclaimed [gothic roguelike](https://radeksprta.eu/darkest-dungeon-new-czech-translation/).
In case you haven't head about it, this RPG is set in
Lovecraftian world. So, it is not for the faint of heart. Your heroes will
starve, despair, get sick and die. And they will stay dead.

## Features

The expansion brings several new features:

  * Wave-Based Survival in a New Environment: The Farmstead! Nonstop action! Fight through endless groups of monsters in an effort to reach the comet’s crash.
  * Resupply! Bolster your party with consumables obtained from mysterious new curios between waves of combat.
  * An All-New Enemy Faction. The Miller and his loyal workmen have become lifeless husks, consumed by the ever-growing crystalline influence that has spread from the comet across the farm...
  * 2 New Bosses 
    * The Miller
    * Thing From the Stars (mini-boss)
  * Twisted New Trinkets. Available for purchase from the Jeweler, a new vendor in the Nomad Wagon. Put your gold away—he trades only in Comet Shards!
  * New tunes! The Farmstead boasts the longest, most involved combat score to date!

I have taken part in the beta testing a I can definitely say you should grab
this one. And while you are at it, why don't you get yourself a copy of
[Crimson Court](https://radeksprta.eu/darkest-dungeon-the-crimson-court-dlc-announced/) as well?
If you still need convincing, check the launch trailer:

{{% media url="https://youtu.be/-ZkmC8ucFkI" %}}
