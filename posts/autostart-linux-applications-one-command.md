<!--
.. title: Autostart Linux Applications in One Command
.. slug: autostart-linux-applications-one-command
.. date: 2016-11-18 23:03:23
.. tags: automatization,autostart,command line,freedesktop
.. category: Linux
.. link: 
.. description: 
.. type: text
.. categories: Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

Usually, when I am done working on the computer, I just suspend it. Thanks to
that, I can easily pick up where I left off. But sometimes, I have to swallow
the pill and turn it off (for example to preserve charge while traveling). In
such cases, you can autostart Linux apps to quickly setup your work
environment and get back to being productive.  Fortunately, you can easily set
this up in desktop environments adhering to
[Freedesktop](https://www.freedesktop.org/wiki/) standards. These include
Gnome and KDE, among others. You just need to copy the .desktop files of your
favorite programs from `/usr/share/applications/` to the autostart directory.
For instance your browser, password manager or a [roll-down
terminal](https://github.com/Guake/guake). If you only want it to start for
yourself, copy it to `$HOME/.config/autostart`. However, for system-wide
configuration, the directory is `/etc/xdg/autostart`. As a result, you don't
have to waste time starting up the same 5 applications every time you reboot.
And you can set everything up using just command line. So you can even use it
is part of your computer installation script. If you are looking for other
ways to automize tedious tasks, check out this [guide to automatically
configure your computer with Ansible](https://radeksprta.eu/automatically-setup-computer-ansible-playbook/).

