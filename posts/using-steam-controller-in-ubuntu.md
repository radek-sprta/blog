<!--
.. title: Using Steam Controller in Ubuntu
.. slug: using-steam-controller-in-ubuntu
.. date: 2016-11-23 15:56:28
.. tags: steam,steam controller,tutorial
.. category: Games
.. link:
.. description:
.. type: text
.. categories: Games,Linux
.. has_math: no
.. status: published
.. wp-status: publish
-->

I have recently picked up the Steam controller. With it, I can finally enjoy
some of the platformers that have been sitting in my Steam library for ages
(such as Never Alone and
[Feist](https://playfeist.net/)). Why Steam controller? The reason is twofold.
Firstly, it seems to be one of best gamepads around according to the reviews.
Secondly, it was also supposed to run out of the box on Linux.  Well, that
might be the case if you connect it by USB (I have not tried that). But in
order to run it in wireless mode, you have to manually install the drivers.
Hopefully, they will be automatically downloaded when you install Steam in
future releases, but for now, you have to run this command.

```text
$ sudo apt install steam-devices
```

Or in OpenSuse:

```text
$ sudo zypper install steam-controller
```

When the installation completes, just fire
up Steam in the Big Picture Mode and add your controller. A bit more work that
I would have hoped for, but still simple enough. I might sum up my impressions
in another blog post. Now I am off to play some games.

## Trouble after update

Edit: If you are experiencing problems with your Steam controller after a
recent update, check out [this blog post](https://radeksprta.eu/posts/fix-steam-controller-issues-linux/).
