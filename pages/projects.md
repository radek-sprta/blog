<!--
.. title: Projects
.. slug: projects
.. date: 2020-05-09 16:57:52 UTC+02:00
.. tags: 
.. category: 
.. link: 
.. description: A selection of my open-source projects.
.. type: text
-->

Here are some of the projects I worked on in my spare time.

## Awesome Games Remakes List
A curated list of actively maintained open-source game remakes.  
[https://gitlab.com/radek-sprta/awesome-game-remakes/](https://gitlab.com/radek-sprta/awesome-game-remakes/)

## Blog Terminal
A plugin for WordPress, that generates terminal-like box, to display terminal output.  
[https://wordpress.org/plugins/blog-terminal/](https://wordpress.org/plugins/blog-terminal/)

## Cachalot
A minimal persistent memoization cache for Python, that uses TinyDB as backend.  
[https://gitlab.com/radek-sprta/Cachalot](https://gitlab.com/radek-sprta/Cachalot)

## cdup
cdup is a Rust shell utility to ascend directories by count or path.
[https://gitlab.com/radek-sprta/cdup](https://gitlab.com/radek-sprta/cdup)

## DNS resolver
Free OpenNIC DNS resolver with no logging and support for DNSSEC and DNS over TLS.  
[https://dns.radeksprta.eu](https://dns.radeksprta.eu)

## Mariner
Command-line torrent searcher with streamlined interface.  
[https://gitlab.com/radek-sprta/mariner](https://gitlab.com/radek-sprta/mariner)

## Powerfish
An elegant and informative prompt for Fish inspired by Powerline.  
[https://gitlab.com/radek-sprta/Powerfish](https://gitlab.com/radek-sprta/Powerfish)

## SSLCheck
A simple command line SSL validator.  
[https://gitlab.com/radek-sprta/SSLCheck](https://gitlab.com/radek-sprta/SSLCheck)

## Vigil
A command-line application that automatically notifies user when a web page changes.  
[https://gitlab.com/radek-sprta/vigil](https://gitlab.com/radek-sprta/vigil)
