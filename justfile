default:
    @just --list

auto:
    uv run nikola auto

post:
    uv run nikola new_post
