# -*- coding: utf-8 -*-
import time
from nikola import filters

# Data about this site
BLOG_AUTHOR = "Radek Sprta"  # (translatable)
BLOG_TITLE = "Radek Sprta"  # (translatable)
# This is the main URL for your site. It will be used
# in a prominent link. Don't forget the protocol (http/https)!
SITE_URL = "https://radeksprta.eu/"
# This is the URL where Nikola's output will be deployed.
# If not set, defaults to SITE_URL
# BASE_URL = "https://radeksprta.eu/"
BLOG_EMAIL = "mail@radeksprta.eu"
# (translatable)
BLOG_DESCRIPTION = "Blog about Linux, Japanese and whatever else I fancy"

# What is the default language?
DEFAULT_LANG = "en"
# What will translated input files be named like?
TRANSLATIONS_PATTERN = "{path}.{lang}.{ext}"

# Links for the sidebar / navigation bar.  (translatable)
NAVIGATION_LINKS = {
    DEFAULT_LANG: (
        ("/pages/about/", "About"),
        ("/", "Blog"),
        ("/pages/projects/", "Projects"),
    ),
}
# Alternative navigation links. Works the same way NAVIGATION_LINKS does,
# although themes may not always support them. (translatable)
# (Bootstrap 4: right-side of navbar, Bootblog 4: right side of title)
NAVIGATION_ALT_LINKS = {DEFAULT_LANG: ()}
# Name of the theme to use.
THEME = "hanami"
# Primary color of your theme. This will be used to customize your theme.
# Must be a HEX value.
THEME_COLOR = "#f1404b"
# Theme configuration. Fully theme-dependent. (translatable)
THEME_CONFIG = {DEFAULT_LANG: {}}
# POSTS and PAGES contains (wildcard, destination, template) tuples.
# (translatable)
POSTS = (
    ("posts/*.md", "posts", "post.tmpl"),
    ("posts/*.rst", "posts", "post.tmpl"),
)
PAGES = (
    ("pages/*.md", "pages", "page.tmpl"),
    ("pages/*.rst", "pages", "page.tmpl"),
)

# Below this point, everything is optional
TIMEZONE = "Europe/Prague"
COMPILERS = {
    "markdown": (".md", ".mdown", ".markdown"),
    "rest": (".rst", ".txt"),
}
HIDDEN_TAGS = ["mathjax"]
TAGLIST_MINIMUM_POSTS = 2
# If CATEGORY_ALLOW_HIERARCHIES is set to True, categories can be organized in
# hierarchies. For a post, the whole path in the hierarchy must be specified,
# using a forward slash ('/') to separate paths. Use a backslash ('\') to escape
# a forward slash or a backslash (i.e. '\//\\' is a path specifying the
# subcategory called '\' of the top-level category called '/').
CATEGORY_ALLOW_HIERARCHIES = True
# If CATEGORY_OUTPUT_FLAT_HIERARCHY is set to True, the output written to output
# contains only the name of the leaf category and not the whole path.
CATEGORY_OUTPUT_FLAT_HIERARCHY = True
HIDDEN_CATEGORIES = []
HIDDEN_AUTHORS = ["Guest"]
FRONT_INDEX_HEADER = {DEFAULT_LANG: ""}
ARCHIVE_PATH = ""
ARCHIVE_FILENAME = "archive/index.html"
# URLs to other posts/pages can take 3 forms:
# rel_path: a relative URL to the current page/post (default)
# full_path: a URL with the full path from the root
# absolute: a complete URL (that includes the SITE_URL)
URL_TYPE = "full_path"
ATOM_FILENAME_BASE = "feed"
# A list of redirection tuples, [("foo/from.html", "/bar/to.html")].
GITHUB_SOURCE_BRANCH = "src"
GITHUB_DEPLOY_BRANCH = "main"
GITHUB_REMOTE_NAME = "origin"
GITHUB_COMMIT_SOURCE = True
FILTERS = {
    ".css": [filters.cssminify],
    ".html": [filters.typogrify, filters.html5lib_minify],
    ".js": [filters.jsminify],
    ".jpg": ["jpegoptim --strip-all -m75 -v %s"],
    ".xml": [filters.xmlminify],
}
HTML_TIDY_EXECUTABLE = "tidy"
IMAGE_FOLDERS = {"images": "images"}

# HTML fragments and diverse things that are used by the templates
# (translatable) If the following is empty, defaults to ' [old posts,] page %d' (see above):
# INDEXES_PAGES = "page %d"
# If the following is True, index-1.html has the oldest posts, index-2.html the
# second-oldest posts, etc., and index.html has the newest posts. This ensures
# that all posts on index-x.html will forever stay on that page, now matter how
# many new posts are added.
INDEXES_STATIC = False
INDEXES_PRETTY_PAGE_URL = ["page", "{number}", "{index_file}"]
# FAVICONS contains (name, file, size) tuples.
# Used to create favicon link like this:
# <link rel="name" href="file" sizes="size"/>
FAVICONS = (("icon", "/assets/icons/favicon.png", "32x32"),)
INDEX_TEASERS = True
# 'Read more...' for the index page, if INDEX_TEASERS is True (translatable)
INDEX_READ_MORE_LINK = """
<p class="more text-right">
<a href="{link}" class="button primary">{read_more}…</a>
</p>
"""
# 'Read more...' for the feeds, if FEED_TEASERS is True (translatable)
FEED_READ_MORE_LINK = '<p><a href="{link}">{read_more}…</a> ({min_remaining_read})</p>'
LICENSE = """
<a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">CC BY-SA 4.0</a>
"""
CONTENT_FOOTER = """
<span class="hide-xs">&copy;{date}  <a href="mailto:{email}">{author}</a>
<span class="hide-sm"> - License {license}
- Powered by <a href="https://getnikola.com" rel="nofollow">Nikola</a>
</span>
</span>
"""
CONTENT_FOOTER_FORMATS = {
    DEFAULT_LANG: (
        (),
        {
            "email": BLOG_EMAIL,
            "author": BLOG_AUTHOR,
            "date": time.gmtime().tm_year,
            "license": LICENSE,
        },
    )
}
RSS_COPYRIGHT = '© {date} <a href="mailto:{email}">{author}</a> {license}'
RSS_COPYRIGHT_PLAIN = "© {date} {author} {license}"
RSS_COPYRIGHT_FORMATS = CONTENT_FOOTER_FORMATS
COMMENT_SYSTEM = ""
COMMENT_SYSTEM_ID = ""
STRIP_INDEXES = True
# List of files relative to the server root (!) that will be asked to be excluded
# from indexing and other robotic spidering. * is supported. Will only be effective
# if SITE_URL points to server root. The list is used to exclude resources from
# /robots.txt and /sitemap.xml, and to inform search engines about /sitemapindex.xml.
ROBOTS_EXCLUSIONS = ["/archive/index.html", "/category/*.html"]
PRETTY_URLS = True
DEPLOY_DRAFTS = False
MARKDOWN_EXTENSIONS = [
    "markdown.extensions.fenced_code",
    "markdown.extensions.codehilite",
    "markdown.extensions.extra",
    # 'markdown_captions',  #  https://github.com/python-poetry/poetry/issues/3320
]
CODE_COLOR_SCHEME = "tango"
# Social buttons. This is sample code for AddThis (which was the default for a
# long time). Insert anything you want here, or even make it empty (which is
# the default right now)
# (translatable)
# SOCIAL_BUTTONS_CODE = """
GENERATE_ATOM = True
SEARCH_FORM = (
    """
<!-- DuckDuckGo custom search -->
<form method="get" id="search" action="https://duckduckgo.com/"
 class="navbar-form pull-left">
<input type="hidden" name="sites" value="%s">
<input type="hidden" name="k7" value="#f4f5f9">
<input type="hidden" name="k8" value="#252c41">
<input type="hidden" name="k9" value="#f1404b">
<input type="hidden" name="kx" value="e">
<input type="hidden" name="kt" value="Glegoo">
<input type="text" name="q" maxlength="255" placeholder="Search&hellip;" id="searchbox">
<input type="submit" value="DuckDuckGo Search" class="is-hidden">
</form>
<!-- End of custom search -->
"""
    % SITE_URL
)
USE_BUNDLES = True
USE_TAG_METADATA = False
WARN_ABOUT_TAG_METADATA = False
GLOBAL_CONTEXT = {
    "github_name": "radek-sprta",
    "gitlab_name": "radek-sprta",
    "linkedin_name": "radek-sprta-317057111",
    "assets_version": int(time.time()),
}
GLOBAL_CONTEXT_FILLER = []

REDIRECTIONS = [
    ["rss", "/rss.xml"],
    ["about/index.html", "/pages/about/"],
    ["projects/index.html", "/pages/projects/"],
    [
        "japanese-cloze-card-generation-plugin-for-anki/index.html",
        "/posts/japanese-cloze-card-generation-plugin-for-anki/",
    ],
    ["facebook-in-kansaiben/index.html", "/posts/facebook-in-kansaiben/"],
    ["cube-and-me/index.html", "/posts/cube-and-me/"],
    [
        "on-seeing-the-100-perfect-girl-one-beautiful-april-morning/index.html",
        "/posts/on-seeing-the-100-perfect-girl-one-beautiful-april-morning/",
    ],
    [
        "nhk-radio-special-tohoku-earthquake/index.html",
        "/posts/nhk-radio-special-tohoku-earthquake/",
    ],
    [
        "attack-on-the-titan-kansaiben-version/index.html",
        "/posts/attack-on-the-titan-kansaiben-version/",
    ],
    ["baldurs-gate-in-japanese/index.html", "/posts/baldurs-gate-in-japanese/"],
    [
        "test-internet-speed-command-line/index.html",
        "/posts/test-internet-speed-command-line/",
    ],
    ["baldurs-gate-expansion/index.html", "/posts/baldurs-gate-expansion/"],
    [
        "automatically-remove-old-kernels-ubuntu/index.html",
        "/posts/automatically-remove-old-kernels-ubuntu/",
    ],
    [
        "setting-up-headless-raspberry-pi-server/index.html",
        "/posts/setting-up-headless-raspberry-pi-server/",
    ],
    [
        "turn-raspberry-pi-into-torrentbox/index.html",
        "/posts/turn-raspberry-pi-into-torrentbox/",
    ],
    [
        "scarlet-johansson-in-ghost-in-the-shell/index.html",
        "/posts/scarlet-johansson-in-ghost-in-the-shell/",
    ],
    [
        "fix-steam-ubuntu-without-hassle/index.html",
        "/posts/fix-steam-ubuntu-without-hassle/",
    ],
    [
        "easily-download-flickr-images/index.html",
        "/posts/easily-download-flickr-images/",
    ],
    [
        "installing-nextcloud-on-raspberry-pi/index.html",
        "/posts/installing-nextcloud-on-raspberry-pi/",
    ],
    [
        "darkest-dungeon-new-czech-translation/index.html",
        "/posts/darkest-dungeon-new-czech-translation/",
    ],
    [
        "darkest-dungeon-the-crimson-court-dlc-announced/index.html",
        "/posts/darkest-dungeon-the-crimson-court-dlc-announced/",
    ],
    [
        "automatically-turn-off-the-leds-on-raspberry-pi/index.html",
        "/posts/automatically-turn-off-the-leds-on-raspberry-pi/",
    ],
    [
        "attack-on-the-titan-board-game/index.html",
        "/posts/attack-on-the-titan-board-game/",
    ],
    [
        "use-pi-hole-block-ads-raspberry-pi/index.html",
        "/posts/use-pi-hole-block-ads-raspberry-pi/",
    ],
    [
        "diggin-in-the-carts-history-of-japanese-game-music/index.html",
        "/posts/diggin-in-the-carts-history-of-japanese-game-music/",
    ],
    [
        "impacts-and-ethics-of-eating-meat/index.html",
        "/posts/impacts-and-ethics-of-eating-meat/",
    ],
    ["20th-anniversary-of-diablo/index.html", "/posts/20th-anniversary-of-diablo/"],
    [
        "automatically-setup-computer-ansible-playbook/index.html",
        "/posts/automatically-setup-computer-ansible-playbook/",
    ],
    [
        "autostart-linux-applications-one-command/index.html",
        "/posts/autostart-linux-applications-one-command/",
    ],
    [
        "using-steam-controller-in-ubuntu/index.html",
        "/posts/using-steam-controller-in-ubuntu/",
    ],
    [
        "managing-vim-plugins-easily-using-git/index.html",
        "/posts/managing-vim-plugins-easily-using-git/",
    ],
    [
        "fix-steam-controller-issues-linux/index.html",
        "/posts/fix-steam-controller-issues-linux/",
    ],
    [
        "protect-eyes-computer-eye-strain/index.html",
        "/posts/protect-eyes-computer-eye-strain/",
    ],
    ["website-obesity/index.html", "/posts/website-obesity/"],
    [
        "powerfish-an-elegant-and-informative-prompt/index.html",
        "/posts/powerfish-an-elegant-and-informative-prompt/",
    ],
    [
        "setup-mosh-sshs-cousin-for-mobile-era/index.html",
        "/posts/setup-mosh-sshs-cousin-for-mobile-era/",
    ],
    [
        "change-raspberry-pi-password-username-hostname/index.html",
        "/posts/change-raspberry-pi-password-username-hostname/",
    ],
    ["raspberry-pi-wifi-setup/index.html", "/posts/raspberry-pi-wifi-setup/"],
    [
        "set-up-password-less-ssh-login-and-secure-your-ssh/index.html",
        "/posts/set-up-password-less-ssh-login-and-secure-your-ssh/",
    ],
    [
        "make-ansible-playbook-distribution-agnostic/index.html",
        "/posts/make-ansible-playbook-distribution-agnostic/",
    ],
    [
        "powerfish-gets-new-features-support-themes/index.html",
        "/posts/powerfish-gets-new-features-support-themes/",
    ],
    [
        "vim-tips-increased-productivity/index.html",
        "/posts/vim-tips-increased-productivity/",
    ],
    [
        "automatic-clamav-scans-with-email-notifications/index.html",
        "/posts/automatic-clamav-scans-with-email-notifications/",
    ],
    [
        "how-to-connect-external-hard-drive-to-raspberry-pi/index.html",
        "/posts/how-to-connect-external-hard-drive-to-raspberry-pi/",
    ],
    [
        "run-docker-container-regular-user/index.html",
        "/posts/run-docker-container-regular-user/",
    ],
    [
        "assign-raspberry-pi-static-ip-couple-steps/index.html",
        "/posts/assign-raspberry-pi-static-ip-couple-steps/",
    ],
    [
        "blog-terminal-wordpress-terminal-plugin/index.html",
        "/posts/blog-terminal-wordpress-terminal-plugin/",
    ],
    [
        "steam-not-working-opensuse-quick-fix/index.html",
        "/posts/steam-not-working-opensuse-quick-fix/",
    ],
    ["why-privacy-matters/index.html", "/posts/why-privacy-matters/"],
    [
        "improve-your-privacy-with-dns-over-tls/index.html",
        "/posts/improve-your-privacy-with-dns-over-tls/",
    ],
    [
        "mariner-command-line-torrent-searcher/index.html",
        "/posts/mariner-command-line-torrent-searcher/",
    ],
    [
        "use-editorconfig-for-your-project/index.html",
        "/posts/use-editorconfig-for-your-project/",
    ],
    [
        "improve-wordpress-load-speed-wp-super-cache/index.html",
        "/posts/improve-wordpress-load-speed-wp-super-cache/",
    ],
    [
        "darkest-dungeon-the-color-of-madness-out/index.html",
        "/posts/darkest-dungeon-the-color-of-madness-out/",
    ],
    [
        "install-minidlna-to-stream-media/index.html",
        "/posts/install-minidlna-to-stream-media/",
    ],
    ["red-meat-is-not-the-culprit/index.html", "/posts/red-meat-is-not-the-culprit/"],
    [
        "use-passphrases-instead-of-passwords/index.html",
        "/posts/use-passphrases-instead-of-passwords/",
    ],
    [
        "set-up-a-wireguard-vpn-in-15-minutes/index.html",
        "/posts/set-up-a-wireguard-vpn-in-15-minutes/",
    ],
    [
        "did-not-find-mosh-server-startup-message/index.html",
        "/posts/did-not-find-mosh-server-startup-message/",
    ],
    ["the-glimpse-controversy/index.html", "/posts/the-glimpse-controversy/"],
    [
        "use-caddy-reverse-proxy-for-kibana/index.html",
        "/posts/use-caddy-reverse-proxy-for-kibana/",
    ],
]
