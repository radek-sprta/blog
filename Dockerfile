FROM python:3.9 AS builder

# Copy uv
COPY --from=ghcr.io/astral-sh/uv:latest /uv /uvx /bin/

# Copy the whole repository into Docker container
COPY . . 

# Build the blog
RUN apt update \
    && apt install -y jpegoptim \
    && uv run nikola build


FROM caddy:2-alpine
LABEL maintainer="Radek Sprta <mail@radeksprta.eu>"

EXPOSE 80
EXPOSE 2019

HEALTHCHECK --interval=10s --retries=3 --start-period=5s \
    CMD  wget --spider --user-agent Healthcheck -T 1 http://localhost/ 2>/dev/null || exit 1

# Copy output to the default caddy directory
COPY --from=builder output /usr/share/caddy

# Copy caddy host configuration
COPY Caddyfile /etc/caddy/Caddyfile
